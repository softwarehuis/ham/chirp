import glob
import os
import pytest
from chirp import directory


@pytest.mark.parametrize('image', glob.glob(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', "*")))
def test_get_radio(image):
    test = os.path.splitext(os.path.basename(image))[0]
    radio_class = directory.get_radio(test)
    assert radio_class is not None


@pytest.mark.skip("Will not test for now")
@pytest.mark.parametrize('image', glob.glob(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', "*")))
def test_get_radio_by_image(image):
    radio_class = directory.get_radio_by_image(image)
    assert radio_class is not None
